import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import LinkRoute from "../components/common/LinkRoute";

function Delete({ id, className }) {
  //   let { id } = useParams();
  const delURL = "http://localhost:5005/deleteusers/" + id;

  const handleonsubmit = async (e) => {
    try {
      const response = await axios.delete(delURL);
      console.log(response);
      window.location.reload();
      window.location = "/";
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <LinkRoute onClick={handleonsubmit}>
        {/* <button type="submit" onClick={handleonsubmit}> */}
        <span className={className}>delete</span>
        {/* </button> */}
      </LinkRoute>
    </>
  );
}

export default Delete;
