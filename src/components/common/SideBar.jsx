import React from "react";

import styled from "@emotion/styled";
import LinkRoute from "./LinkRoute";
import Add from "../../svg/Add";

const SideBarStyled = styled.div`
  width: 50px;
  position: fixed;
  height: 100%;
  top: 0;
  left: 0;
  overflow: visible;
  z-index: 1030;
  transition: all 0.3s ease;
  background: #ffffff;
  box-shadow: 0px 4px 18px rgba(0, 0, 0, 0.06);
`;

const UlStyled = styled.ul`
  list-style: none;
  padding: 7px;
  margin: 0;

  .material-symbols-outlined {
    font-size: 35px;
    margin-top: 10px;
    color: black;
  }
`;

const ImgStyled = styled.img`
  border-radius: 50%;
  width: 35px;
  display: block;
  max-width: 100%;
  height: auto;
  padding: 8px 0;
`;

function SideBar() {
  const { MaterialIcon } = "";
  const handleClick = (e) => {
    e.preventDefault();
  };
  return (
    <SideBarStyled>
      <UlStyled>
        <LinkRoute to="/">
          <ImgStyled
            src="https://media.istockphoto.com/id/1176363686/vector/smiling-young-asian-girl-profile-avatar-vector-icon.jpg?s=612x612&w=0&k=20&c=QuyZJNKexFQgDPr9u91hKieWKOYbaFxPb0b0gwmd-Lo="
            alt="profile image"
          />
        </LinkRoute>

        {/* <LinkRoute to="/">
          <span class="material-symbols-outlined">add_circle</span>
        </LinkRoute> */}

        {/* <LinkRoute to="/about">DELETE</LinkRoute>

        <LinkRoute to="/contact">EDIT</LinkRoute>

        <LinkRoute>test</LinkRoute> */}
      </UlStyled>
    </SideBarStyled>
  );
}

export default SideBar;
