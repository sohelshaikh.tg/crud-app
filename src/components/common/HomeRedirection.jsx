import React from "react";
import LinkRoute from "./LinkRoute";
import styled from "@emotion/styled";

const WrapperStyled = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 0 0 16px 0;
  .link-class {
    text-decoration: none;
    color: black;
  }
`;

function HomeRedirection() {
  return (
    <WrapperStyled>
      <LinkRoute
        to="/"
        className="d-flex justify-content-center align-items-center link-class"
      >
        <span class="material-symbols-outlined">cottage</span>
        &nbsp; Home
      </LinkRoute>
    </WrapperStyled>
  );
}

export default HomeRedirection;
