import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  BrowserRouter,
} from "react-router-dom";
import Home from "./pages/Home";
import "bootstrap/dist/css/bootstrap.css";
import Create from "./pages/Create";
import EditUser from "./pages/EditUser";
import UserBio from "./pages/UserBio";
import SideBar from "./components/common/SideBar";
import Nav from "./components/common/NavFooter";
import Header from "./components/common/Header";

function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <SideBar />
        <Header />
        <Routes>
          <Route exact path="/" element={<Home />}></Route>
          <Route exact path="/create" element={<Create />}></Route>
          <Route exact path="/userbio/:id" element={<UserBio />}></Route>
          <Route exact path="/editusers/:id" element={<EditUser />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
